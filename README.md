# skincatalog

## Requirements

* Docker
* poetry Python package

## Development

Execute the following command in your terminal:

    make dev

Once the development server is running head to https://localhosts:8000 in your browser.


## testing

    make test
    
    make coverage
