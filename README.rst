`Local project setup`

install poetry::

    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
    source ~/.poetry/env
    poetry install

update db::

    poetry run flask db upgrade

Run dev webserver locally::

    python manage.py runserver --settings=mysite.settings.development

Coverage::

    DJANGO_SETTINGS_MODULE=skincatalog.settings.development poetry run coverage run --source='.' manage.py test
    DJANGO_SETTINGS_MODULE=skincatalog.settings.development poetry run coverage report

Test::
    DJANGO_SETTINGS_MODULE=skincatalog.settings.development poetry run manage.py test



`Docker`

build::

    docker build -t piportal:latest .

launch with database::

    docker run --name skincatalog -d -p 8000:5000 --rm -e SECRET_KEY=my-secret-key \
        -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true \
        -e MAIL_USERNAME=<your-gmail-username> -e MAIL_PASSWORD=<your-gmail-password> \
        --link mysql:dbserver \
        -e DATABASE_URL=mysql+pymysql://skinportal:<database-password>@dbserver/piportal \
        skinportal:latest

The container will run database migration upgrades at launch

launch with local database and email::

 docker run -it -p 8020:8020 \
     -e DJANGO_SUPERUSER_USERNAME=admin \
     -e DJANGO_SUPERUSER_PASSWORD=sekret1 \
     -e DJANGO_SUPERUSER_EMAIL=admin@example.com \
     -e SECRET_KEY=busdlkjj45l3k
     -e DJANGO_SETTINGS_MODULE=skincatalog.settings.development
     -e DATABASE_URL=sqlite:///db.sqlite3
     -e ALLOWED_HOSTS=localhost,127.0.0.1
     -e WSGI_SCRIPT=skincatalog.wsgi
     -e RECAPTCHA_SECRET_KEY=abcdefej12345
     registry.gitlab.com/skincatalog/skincatalog:latest



Add new metadata table etc::

    poetry run python manage.py makemigrations <module>
    poetry run python manage.py migrate


`Deployments`

Several vars are needed in production.
STATIC_ROOT
#     -e STATIC_ROOT=/var/www/webroot/ROOT/static

the config uses ENV to set values in production::

    SECRET_KEY=rqr_cjv4igscyu8&&(0ce(=sy=f2)p=f_wn&@0xsp7m$@!kp=d
    ALLOWED_HOSTS=.djangoboards.com
    DATABASE_URL=postgres://u_boards:BcAZoYWsJbvE7RMgBPzxOCexPRVAq@localhost:5432/django_boards
    GOOGLE_ANALYTICS = 'UA-12345678'

e.g. postgres://db_user:db_password@db_host:db_port/db_name
