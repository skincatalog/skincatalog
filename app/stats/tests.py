from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse

from stats.views import get_stats


class DashboardTests(TestCase):
    def setUp(self):
        url = reverse('stats_home')
        self.response = self.client.get(url)

    def test_dashboard_view_status_code(self):
        self.assertEqual(self.response.status_code, 200)


class DashboardJsonTests(TestCase):
    def setUp(self):
        url = reverse('stats_json')
        self.response = self.client.get(url)

    def test_dashboard_json_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_no_registered_user(self):
        self.assertEqual(0, get_stats().get('registered_users', 1))


class DashboardUserTests(TestCase):
    data = {
        'registered_users': 1,
        'active_users': 0,
        'published_products': 0,
    }

    def setUp(self):
        self.user = User.objects.create_superuser(username='john', email='john@doe.com', password='old_password', )
        # dummy request object as it is required with django-axes
        super().setUp()


class DashboardUserOnly(DashboardUserTests):
    def test_user_registered(self):
        self.assertEqual(self.data, get_stats())


class DashboardLoggedIn(DashboardUserTests):
    def setUp(self):
        super().setUp()
        self.request = HttpRequest()
        self.client.login(request=self.request, username='john', password='old_password')

    def test_user_logged_in(self):
        data = self.data.copy()
        data['active_users'] = 1
        self.assertEqual(data, get_stats())
