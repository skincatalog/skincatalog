from django.contrib.auth.models import User
from django.conf import settings
from django.http import JsonResponse

from django.shortcuts import render
from django.utils import timezone

if 'user_sessions' in settings.INSTALLED_APPS:
    from user_sessions.models import Session
else:
    from django.contrib.sessions.models import Session

from accounts.models import Profile
from catalog.models import Product


def get_all_logged_in_users():
    # Query all non-expired sessions
    # use timezone.now() instead of datetime.now() in latest versions of Django
    sessions = Session.objects.filter(expire_date__gte=timezone.now())
    uid_list = []

    # Build a list of user ids from that query
    for session in sessions:
        data = session.get_decoded()
        uid_list.append(data.get('_auth_user_id', None))

    # Query all logged in users based on id list
    return User.objects.filter(id__in=uid_list)


def get_stats():
    registered_users = Profile.objects.filter(user__is_active=True).count()
    active_users = get_all_logged_in_users().count()
    published_products = Product.objects.filter(state='published').count()

    return {
          'registered_users': registered_users,
          'active_users': active_users,
          'published_products': published_products,
        }


def dashboard(request):
    template_name = 'stats/dashboard.html'
    return render(request, template_name, get_stats())


def dashboard_json(request):
    return JsonResponse(get_stats())
