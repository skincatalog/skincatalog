from django.urls import path, include

from . import views

urlpatterns = [
    path("",
         views.dashboard, name="stats_home"),
    path("json/",
         views.dashboard_json, name="stats_json"),
]
