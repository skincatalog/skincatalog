from django.template import Template, Context
from django.test import TestCase

from external_urls.templatetags.external_urls import external_url


class ExternalUrlViewsTest(TestCase):

    def test_external_link_view_redirect(self):
        """test ExternalLinkView redirect to correct URL"""

        url = 'https://example.com/page/1'
        encoded_url = external_url(url)
        response = self.client.get(encoded_url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, url)


class ExternalUrlTemplateTagTest(TestCase):

    def test_external_url(self):
        """test external_url template tag"""

        template = Template(
            '''{% load external_urls %}
               <html>
               {% external_url 'https://example.com/' %}
               </html>
            '''
        )
        html = template.render(Context())
        self.assertIn('/goto/2TQQJ2bU7mR6MzeqcaihoRsECWUv', html)
