import base58
from django.urls import reverse_lazy
from django import template

register = template.Library()


@register.simple_tag()
def external_url(url):
    """Trap external click on external"""
    url = base58.b58encode(bytes(url, 'utf-8')).decode().strip('\n')
    return reverse_lazy('external_link', kwargs={'external_url': url})
