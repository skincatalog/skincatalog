from django.apps import AppConfig


class ExternalUrlsConfig(AppConfig):
    name = 'external_urls'
