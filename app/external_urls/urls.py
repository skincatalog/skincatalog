from django.urls import path

from . views import ExternalLinkView

urlpatterns = [
    path('goto/<external_url>/', ExternalLinkView.as_view(), name='external_link')
]
