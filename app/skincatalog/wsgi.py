"""
WSGI config for skincatalog project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

virtenv = os.path.expanduser('~') + '/virtenv/'
virtualenv = os.path.join(virtenv, 'bin/activate_this.py')
try:
    exec(compile(open(virtualenv,"rb").read(), virtualenv, 'exec'),dict(__file__=virtualenv))
except IOError:
    pass

os.sys.path.append(os.path.expanduser('~'))
os.sys.path.append(os.path.expanduser('~') + '/ROOT/')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'skincatalog.settings.production')
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
