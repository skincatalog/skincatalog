"""skincatalog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from catalog.urls import views as catalog_views

urlpatterns = [
    path('catalog/', include('catalog.urls')),
    path('boards/', include('boards.urls')),
    path('accounts/', include('accounts.urls')),
    path('membership/', include('membership.urls')),
    # TODO fix with proper home
    path('', catalog_views.ProductSearchView.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('privacy/', catalog_views.privacy, name='privacy'),
    path('dashboard/', include('stats.urls')),
    path('', include('external_urls.urls'))
]

# Do not import anything for the handler404,
# or whatnot from the django.conf.urls
# Just list them below

handler404 = 'catalog.views.not_found'
handler500 = 'catalog.views.server_error'
handler403 = 'catalog.views.permission_denied'
handler400 = 'catalog.views.bad_request'

# enable local serving of MEDIA (uploaded files)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
