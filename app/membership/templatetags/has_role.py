from django import template
from django.contrib import messages
from django.contrib.auth.models import User

from membership.models import Membership, Member

register = template.Library()


@register.filter(name='has_role')
def has_role(user: User, role_name):
    if not user.is_authenticated:
        return False

    if not hasattr(user, 'member'):
        try:
            # a user with no member, lets fix it
            membership = Membership.objects.get(pk=1)
            member = Member(user=user)
            member.membership = membership
            member.save()
        except Member.DoesNotExist:
            return False
    return user.member.has_role(role_name)
