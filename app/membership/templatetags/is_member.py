from django import template

from membership.models import Membership

register = template.Library()


@register.filter(name='is_member')
def is_member(user, member_name):
    try:
        membership = Membership.objects.get(name=member_name)
    except Membership.DoesNotExist:
        return False

    if user.is_staff or user.is_superuser:
        return True
    if user.member.membership == membership:
        return True

    return False

