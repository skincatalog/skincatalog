# Generated by Django 3.0.5 on 2020-04-25 08:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('membership', '0004_membership_enabled'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membership',
            name='long_description',
            field=models.TextField(blank=True, default='', max_length=500),
        ),
    ]
