from django.contrib import admin

from membership.models import Membership, Member, MembershipRole

admin.site.register(Membership)
admin.site.register(Member)
admin.site.register(MembershipRole)
