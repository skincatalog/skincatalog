from django.contrib.auth.models import User
from django.db import models


class Membership(models.Model):
    name = models.CharField(max_length=20, unique=True)
    description = models.TextField(max_length=60, default="")
    long_description = models.TextField(max_length=500, default="", blank=True)
    group_name = models.CharField(max_length=50, default="")
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=8)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    membership = models.ForeignKey(Membership, on_delete=models.CASCADE, related_name='member', default=1)
    # other fields...

    def __str__(self):
        return self.user.username

    def has_role(self, role):
        return True if self.membership in Membership.objects.filter(role__name=role) else False


class MembershipRole(models.Model):
    name = models.CharField(max_length=50, unique=True)
    memberships = models.ManyToManyField(Membership, related_name='role')

    def __str__(self):
        return self.name
