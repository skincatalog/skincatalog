from django.urls import path, include
from . import views

urlpatterns = [
    path("",
         views.MembershipView.as_view(), name="membership_home"),
    path("select/<int:pk>",
         views.select_membership, name="select_membership"),
]
