from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.contrib.auth.models import Group

from membership.models import Membership, Member


def add_member(user, membership_id):
    try:
        membership = Membership.objects.get(pk=membership_id)
    except Membership.DoesNotExist:
        return False
    try:
        user.member.membership = membership
    except Member.DoesNotExist:
        member = Member(user=user)
        member.membership = membership
        member.save()
        user.member = member
    user.member.save()
    return membership


@method_decorator(login_required, name='dispatch')
class MembershipView(ListView):
    model = Membership
    context_object_name = 'membership'
    template_name = 'membership/membership.html'
    paginate_by = 3

    def get_queryset(self):
        queryset = Membership.objects.all().filter(enabled=True).order_by('price')
        return queryset


@login_required
def select_membership(request, pk):
    # check the membership exists
    user = request.user
    membership = add_member(user, pk)
    if membership is False:
        return redirect('membership_home')

    messages.info(request, f'Membership now {pk}, {membership}')

    return redirect('my_account')
