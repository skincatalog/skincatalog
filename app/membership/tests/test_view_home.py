from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse, resolve

from ..models import Membership
from ..views import select_membership, MembershipView, add_member


class HomeTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='john', email='john@doe.com', password='123')
        # dummy request object as it is required with django-axes
        self.request = HttpRequest()
        self.client.login(request=self.request, username='john', password='123')


class LoginRequiredTests(TestCase):
    def test_redirection(self):
        url = reverse('membership_home')
        login_url = reverse('login')
        response = self.client.get(url)
        self.assertRedirects(response, f'{login_url}?next={url}')


class HomeTestsLoggedIn(HomeTests):
    def setUp(self):
        url = reverse('membership_home')
        self.response = self.client.get(url)
        super().setUp()

    def test_home_url_resolves_home_view(self):
        view = resolve('/membership/')
        self.assertEquals(view.func.view_class, MembershipView)


class SelectMembershipFunction(HomeTests):
    def setUp(self):
        super().setUp()

    def test_select_membership_error(self):
        self.assertFalse(add_member(self.user, 5))

    def test_select_membership_add(self):
        membership = add_member(self.user, 1)
        self.assertTrue(self.user.member.membership_id, 1)


class SelectMembership(HomeTests):
    def setUp(self):
        super().setUp()

    def test_update_membership_success_redirect(self):
        url = reverse('select_membership', kwargs={'pk': 1})
        redirect_url = reverse('my_account')
        response = self.client.get(url)
        self.assertRedirects(response, f'{redirect_url}')

    def test_update_membership_resolves_view(self):
        view = resolve('/membership/select/1')
        self.assertEquals(view.func, select_membership)

    def test_update_membership_not_found_redirect(self):
        url = reverse('select_membership', kwargs={'pk': 99})
        redirect_url = reverse('membership_home')
        response = self.client.get(url)
        self.assertRedirects(response, f'{redirect_url}')
