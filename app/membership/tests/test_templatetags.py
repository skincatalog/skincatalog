from django.test import TestCase

from membership.models import Membership
from membership.templatetags.has_role import has_role
from membership.templatetags.is_member import is_member
from membership.tests.test_view_home import HomeTests
from membership.views import add_member


class TemplateTagsMemberTests(HomeTests):
    def setUp(self):
        super().setUp()
        membership = Membership.objects.first()

        self.membership_id = membership.id
        self.membership = add_member(self.user, self.membership_id)

    def test_user_membership(self):
        self.assertEqual(self.user.member.membership.id, self.membership_id)

    def test_membership_missing(self):
        self.assertFalse(is_member(self.user, 'missing'))

    def test_membership_negative(self):
        self.assertFalse(is_member(self.user, Membership.objects.get(pk=2).name))

    def test_membership_okay(self):
        self.assertTrue(is_member(self.user, self.membership.name))

    def test_member_username(self):
        self.assertEqual(str(self.user.member), self.user.username)

    def test_memberships_name(self):
        self.assertEqual(str(self.membership.role.first()), 'moderated')


class TemplateTagsStaffTests(HomeTests):
    def setUp(self):
        super().setUp()
        self.membership = Membership.objects.first()
        self.user.is_staff = True
        self.user.save()

    def test_membership_super_missing(self):
        self.assertFalse(is_member(self.user, 'missing'))

    def test_membership_super_available(self):
        self.assertTrue(is_member(self.user, self.membership.name))


class TemplateTagsRoleTests(HomeTests):
    def setUp(self):
        super().setUp()

    def test_no_member_object(self):
        self.assertFalse(has_role(self.user, 'editor'))
