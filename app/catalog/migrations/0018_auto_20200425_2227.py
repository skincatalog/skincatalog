# Generated by Django 3.0.5 on 2020-04-25 21:27

from django.db import migrations, models
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0017_auto_20200415_1722'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='state',
            field=django_fsm.FSMField(default='new', max_length=50, protected=True),
        ),
        migrations.AddField(
            model_name='product',
            name='state',
            field=django_fsm.FSMField(default='new', max_length=50, protected=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=70, unique=True),
        ),
    ]
