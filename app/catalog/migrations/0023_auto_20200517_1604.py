# Generated by Django 3.0.5 on 2020-05-17 15:04

import catalog.models
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0022_auto_20200509_2320'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='created_at',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='ingredient',
            name='updated_at',
            field=catalog.models.AutoDateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='product',
            name='created_at',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='product',
            name='updated_at',
            field=catalog.models.AutoDateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='buylink',
            name='name',
            field=models.CharField(choices=[('Official', 'Official'), ('Amazon', 'Amazon'), ('eBay', 'eBay'), ('Other', 'Other')], default='Official', max_length=50),
        ),
    ]
