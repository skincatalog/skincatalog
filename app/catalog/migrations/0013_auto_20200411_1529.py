# Generated by Django 3.0.5 on 2020-04-11 14:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0012_altnames'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Altnames',
            new_name='Altname',
        ),
    ]
