from datetime import date

from catalog.models import Search


def inc_search(q):
    query, created = Search.objects.get_or_create(
        q=q,
        date=date.today()
    )
    # if we just created it, count will be zero, increment
    query.count += 1
    query.save()
    return
