from itertools import count

from django import template
from django.utils.safestring import mark_safe

from catalog.models import Ingredient

register = template.Library()


@register.filter(name='alt_names', is_safe=True)
def alt_names(ingredient_pk):
    ingredient = Ingredient.objects.get(id=ingredient_pk)
    if ingredient is None:
        return ""

    altnames = ingredient.altname.all()
    if altnames.count() == 0:
        alt_name_string = 'No other names found'
    else:
        alt_name_string = ', '.join([n.name for n in altnames])
    return mark_safe(f'data-toggle="popover" title="{ingredient.name}" data-content="{alt_name_string}"')
