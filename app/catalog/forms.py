from django import forms

from catalog.models import Search, Product
from catalog.widgets import SearchableSelect


class SearchForm(forms.ModelForm):
    class Meta:
        model = Search
        fields = ['q', ]
        labels = {
             'q': ''
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ('owner', 'group', 'state', 'views')
        widgets = {
            'ingredients': SearchableSelect(
                model='catalog.Ingredient',
                template_name='catalog/widget/ingredient_select.html',
                many=True,
                limit=10)
        }
