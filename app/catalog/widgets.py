from django import forms
from django.apps import apps
from django.template.loader import render_to_string
from django.utils.datastructures import MultiValueDict

get_model = apps.get_model
DICT_TYPES = (MultiValueDict,)


class SearchableSelect(forms.CheckboxSelectMultiple):
    class Media:
        css = {
            'all': (
                'css/searchableselect/main.css',
            )
        }
        js = (
            'js/searchableselect/typeahead.bundle.min.js',
            'js/searchableselect/main.js',
        )

    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model')
        self.many = kwargs.pop('many', True)
        self.limit = int(kwargs.pop('limit', 10))
        self.template_name = kwargs.pop('template_name')

        super(SearchableSelect, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, renderer=None, choices=()):
        if value is None:
            value = []

        if not isinstance(value, (list, tuple)):
            # This is a ForeignKey field. We must allow only one item.
            value = [value]

        values = get_model(self.model).objects.filter(pk__in=value)
        final_attrs = self.build_attrs(attrs, extra_attrs={'name': name})

        return render_to_string(self.template_name, dict(
            field_id=final_attrs['id'],
            field_name=final_attrs['name'],
            field_class=final_attrs.get('class', ''),
            values=values,
            limit=self.limit,
            many=self.many
        ))

    def value_from_datadict(self, data, files, name):
        if self.many and isinstance(data, DICT_TYPES):
            return data.getlist(name)
        return data.get(name, None)
