from datetime import date
from django.contrib.auth.models import Group
from django.contrib.contenttypes.fields import GenericRelation
from django.db.models import Q, ExpressionWrapper, BooleanField
from django.utils import timezone
from django_fsm import FSMField, transition
from star_ratings.models import Rating

from accounts.models import Profile
from django.db import models
from filer.fields.image import FilerImageField
from taggit.managers import TaggableManager

from membership.models import Membership


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


class Category(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class Manufacturer(models.Model):
    name = models.CharField(max_length=50, unique=True)
    url = models.URLField(blank=True)
    address1 = models.CharField(max_length=50, blank=True)
    address2 = models.CharField(max_length=50, blank=True)
    city = models.CharField(max_length=40, blank=True)
    country = models.CharField(max_length=40, blank=True)

    def __str__(self):
        return self.name


def can_approve(instance, user):
    return user.is_superuser or user.is_staff


def can_publish(instance, user):
    return user.member.has_role('publisher')


def is_moderated(instance, user):
    return user.member.has_role('moderated')


def can_delete(instance, user):
    return can_approve(instance=instance, user=user)


def is_product_editor(instance, user):
    if not user.is_authenticated:
        return False
    if user.member.has_role('product_editor'):
        return True
    if can_approve(instance=instance, user=user):
        return True
    return False


def get_ingredients(query, limit=10):
    ingredient_list = Ingredient.objects.filter(name__icontains=query).distinct()[:limit]
    alt_name_list = Ingredient.objects.filter(altname__name__icontains=query).distinct()[:limit]
    # we can merge query sets of same object https://docs.djangoproject.com/en/3.0/ref/models/querysets/#or
    ingredients = ingredient_list | alt_name_list
    return ingredients


# get_altnames_by_ingredient_id:
# returns a list of { ingredient_id, name } where name is altname or ingredient.name
def get_ingredient_search_results(query, limit=10):
    # Encapsulate the comparison expression.
    expression = Q(name__startswith=query)
    # Wrap the expression to specify the field type.
    is_match = ExpressionWrapper(expression, output_field=BooleanField())

    # Annotate each object with the comparison.
    # data = ingredients.annotate(my_field=is_match)
    # Order by the annotated field in reverse, so `True` is first (0 < 1).
    # data = data.order_by('-my_field')

    ingredients = Ingredient.objects.filter(name__icontains=query)\
        .values_list('pk', 'name').annotate(close=is_match).order_by('-close')[:limit]
    alt_names = Altname.objects.filter(name__icontains=query)\
        .values_list('ingredient_id', 'name').annotate(close=is_match).order_by('-close')[:limit]

    ingredient_list = [
        dict(pk=v[0], name=v[1])
        for v
        in ingredients
    ]
    alt_names_list = [
        dict(pk=v[0], name=v[1])
        for v
        in alt_names
    ]
    return ingredient_list + alt_names_list


class Ingredient(models.Model):
    name = models.CharField(max_length=70, unique=True)
    description = models.TextField(max_length=1023, blank=True)
    state = FSMField(default='new', protected=True)
    created_at = models.DateField(default=timezone.now)
    updated_at = AutoDateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

    @transition(field=state, source='new', target='published', permission=can_publish)
    def publish(self):
        pass

    @transition(field=state, source='*', target='moderated')
    def moderate(self):
        pass

    @transition(field=state, source='moderated', target='published',
                permission=can_approve)
    def approve(self):
        pass

    @transition(field=state, source='*', target='deleted', permission=can_delete)
    def remove(self):
        pass


class Altname(models.Model):
    name = models.CharField(max_length=70, unique=True)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE, related_name='altname')

    def __str__(self):
        return self.name


def get_default_image():
    from filer.models.imagemodels import Image
    try:
        return Image.objects.filter(name='Default Image').order_by('-uploaded_at')[0].pk
    except IndexError:
        return None


class Product(models.Model):
    name = models.CharField(max_length=70, unique=True)
    description = models.TextField(max_length=1023, blank=True)
    image = FilerImageField(on_delete=models.CASCADE, null=True, blank=True,
                            default=get_default_image, related_name='product_image')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='products')
    views = models.PositiveIntegerField(default=0)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, related_name='products')
    ingredients = models.ManyToManyField('Ingredient')
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True, related_name='products')
    group = models.ForeignKey(Group, on_delete=models.PROTECT, null=True, related_name='products')
    ratings = GenericRelation(Rating, related_query_name='products')
    state = FSMField(default='new', protected=True)
    created_at = models.DateField(default=timezone.now)
    updated_at = AutoDateTimeField(default=timezone.now)
    tags = TaggableManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('catalog_product', args=[str(self.id)])

    @transition(field=state, source='new', target='published', permission=can_publish)
    def publish(self):
        pass

    @transition(field=state, source='*', target='moderated')
    def moderate(self):
        pass

    @transition(field=state, source='moderated', target='published',
                permission=can_approve)
    def approve(self):
        pass

    @transition(field=state, source='*', target='deleted', permission=can_delete)
    def remove(self):
        pass


class BuyLink(models.Model):
    class LinkType(models.TextChoices):
        OFFICIAL = 'Official', 'Official'
        AMAZON = 'Amazon', 'Amazon'
        EBAY = 'eBay', 'eBay'
        OTHER = 'Other', 'Other'

    name = models.CharField(
        max_length=50,
        choices=LinkType.choices,
        default=LinkType.OFFICIAL
    )
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='links')
    url = models.URLField(unique=True)
    clicks = models.PositiveIntegerField(default=0)
    rating = models.FloatField(default=0.0)

    def __str__(self):
        return self.product.name + " " + self.name


class Search(models.Model):
    q = models.CharField(max_length=30)
    date = models.DateField(default=date.today)
    count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.q
