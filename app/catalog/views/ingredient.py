from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import ListView, DetailView
from django_fsm import can_proceed

from catalog.models import Product, Ingredient, get_ingredient_search_results


def filter_ingredients(request):
    value = request.GET.get('q')
    limit = int(request.GET.get('limit', 10))

    values = get_ingredient_search_results(value, limit)
    return JsonResponse(dict(result=values))


class IngredientListView(ListView):
    model = Ingredient
    context_object_name = 'ingredients'
    template_name = 'catalog/ingredient_list.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return Ingredient.objects.all().order_by('-name')


class IngredientModeratedListView(UserPassesTestMixin, ListView):
    model = Ingredient
    context_object_name = 'ingredients'
    template_name = 'catalog/ingredient_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Ingredient.objects.all().filter(state='moderated').order_by('-name') | \
               Ingredient.objects.all().filter(state='new').order_by('-name')

    def test_func(self):
        return self.request.user.is_staff


class IngredientView(DetailView):
    model = Ingredient
    template_name = 'catalog/ingredient_full.html'


def publish_ingredient_view(request, ingredient_id):
    ingredient = get_object_or_404(Ingredient, pk=ingredient_id)
    if not can_proceed(ingredient.publish):
        messages.info(request, "Access Denied")
        return redirect('catalog_ingredient', ingredient.pk)

    ingredient.publish()
    ingredient.save()
    messages.info(request, "Ingredient Published")
    return redirect('catalog_ingredient', ingredient.pk)


class ProductByIngredientView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'catalog/ingredient_full.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        kwargs['ingredient'] = self.ingredient
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.ingredient = get_object_or_404(Ingredient, pk=self.kwargs.get('pk'))
        queryset = self.ingredient.product_set.filter(state='published').order_by('-name')
        return queryset
