from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django_fsm import can_proceed
from filer.models import Image
from taggit.models import Tag

from catalog.forms import ProductForm
from catalog.models import Product, Category, can_approve, Manufacturer, is_product_editor, is_moderated
from catalog.views import Buttons


class ProductByBaseView(ListView):
    model = Product
    context_object_name = 'products'
    paginate_by = 20


class ProductDetailListView(ProductByBaseView):
    paginate_by = 5
    template_name = 'catalog/product_detail_list.html'

    def get_queryset(self):
        return Product.objects.all().filter(state='published').order_by('-updated_at')


class ProductHome(ProductDetailListView):
    template_name = 'catalog/home.html'

    # limited to 5 results for the home page
    def get_queryset(self):
        return Product.objects.all().filter(state='published').order_by('-updated_at')[:5]


class ProductOwnerListView(LoginRequiredMixin, ProductByBaseView):
    template_name = 'catalog/product_list.html'

    def get_queryset(self):
        queryset = self.request.user.profile.products.filter(state='new') | self.request.user.profile.products.filter(
            state='moderated')

        return queryset.order_by('-name')


class ProductModeratedListView(UserPassesTestMixin, ProductByBaseView):
    template_name = 'catalog/product_list.html'

    def get_queryset(self):
        return Product.objects.all().filter(state='moderated').order_by('-name') | \
               Product.objects.all().filter(state='new').order_by('-name')

    def test_func(self):
        return self.request.user.is_staff


class ProductView(DetailView):
    model = Product
    template_name = 'catalog/product_full.html'

    def get_buttons(self):
        buttons = Buttons
        if not self.request.user.is_authenticated:
            return buttons
        product = self.object
        user = self.request.user

        if product.owner == user.profile:
            if user.member.has_role("product_editor"):
                buttons.show_state = True
                buttons.show_edit = True

            if product.state != 'published' and user.member.has_role("product_publisher"):
                buttons.show_publish = True
            if product.state == 'new' and buttons.show_publish is False:
                buttons.show_submit = True

        if user.is_staff:
            buttons.show_state = True
            buttons.show_edit = True

            if product.state != 'published':
                buttons.show_publish = True
            if product.state != 'moderated':
                buttons.show_moderate = True

        return buttons

    def get_context_data(self, **kwargs):
        session_key = 'viewed_product_{}'.format(self.object.pk)
        if not self.request.session.get(session_key, False):
            self.object.views += 1
            self.object.save()
            self.request.session[session_key] = True

        kwargs['product'] = self.object
        kwargs['star_ratings_template_name'] = 'catalog/rating_widget.html'
        kwargs['buttons'] = self.get_buttons()
        return super().get_context_data(**kwargs)

    # limit get_object by defining queryset
    def get_queryset(self):
        my_query_set = Product.objects.all().filter(state='published')
        if self.request.user.is_authenticated:
            if can_approve(Product(), self.request.user):
                return Product.objects.all().order_by('-name')

            if self.request.user.member.has_role('product_editor'):
                return my_query_set | Product.objects.all().filter(owner=self.request.user.profile)

        return my_query_set


def publish_product_view(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    message = "You cannot do that"
    if can_proceed(product.approve):
        product.approve()
        message = "Product Approved"
    else:
        if can_proceed(product.publish):
            product.publish()
            message = "Product Published"
        else:
            if can_proceed(product.moderate):
                product.moderate()
                message = "Product sent for Moderation"

    product.save()
    messages.info(request, message)
    return redirect('catalog_product', product.pk)


def moderate_product_view(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    product.moderate()
    product.save()
    messages.info(request, "Product Moderated")
    return redirect('catalog_product', product.pk)


class ProductByCategoryView(ProductByBaseView):
    template_name = 'catalog/product_by_category.html'

    def get_context_data(self, **kwargs):
        kwargs['category'] = self.category
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.category = get_object_or_404(Category, pk=self.kwargs.get('pk'))
        queryset = self.category.products.filter(state='published').order_by('-name')
        return queryset


class ProductByManufacturer(ProductByBaseView):
    template_name = 'catalog/product_by_manufacturer.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        kwargs['manufacturer'] = self.manufacturer
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.manufacturer = get_object_or_404(Manufacturer, pk=self.kwargs.get('pk'))
        return self.manufacturer.products.filter(state='published').order_by('-name')


class ProductByTagView(ProductByBaseView):
    template_name = 'catalog/product_by_tag.html'

    def get_context_data(self, **kwargs):
        kwargs['tag'] = self.tag
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.slug = self.kwargs.get('tagslug')
        self.tag = Tag.objects.filter(slug=self.slug).values_list('name', flat=True).first()
        queryset = Product.objects.filter(tags__name__in=[self.tag]).order_by('-name')
        return queryset


def get_image(uploaded_file, user):
    return Image.objects.get_or_create(
        name=str(uploaded_file), is_public=True, file=uploaded_file,
        description='Product Image', owner=user
    )


class ProductCreate(UserPassesTestMixin, CreateView):
    model = Product
    template_name = 'catalog/product_edit.html'
    form_class = ProductForm

    def form_valid(self, form):
        form.instance.owner = self.request.user.profile
        form.instance.group = Group.objects.get(name='ProductCreators')
        form.instance.views = 0
        return super().form_valid(form)

    def test_func(self):
        return is_product_editor(Product, self.request.user)


class ProductEdit(UserPassesTestMixin, UpdateView):
    model = Product
    template_name = 'catalog/product_edit.html'
    form_class = ProductForm

    def form_valid(self, form):
        if form.instance.group is None:
            form.instance.group = Group.objects.get(name='ProductCreators')

        # # shim to enable image to be added if not user.is_staff
        # if 'uploaded_image' in form.changed_data:
        #     form.instance.image = get_image(
        #         form.cleaned_data['uploaded_image'],
        #         self.request.user
        #     )

        # need to check if product was published, but a user edited
        # the product and doesn't have publish rights
        product = form.save()
        if 'description' in form.changed_data and is_moderated(Product, self.request.user):
            product.moderate()
            product.save()

        return super().form_valid(form)

    def test_func(self):
        return is_product_editor(Product, self.request.user)
