# Error Pages
from django.shortcuts import render


def server_error(request):
    return render(request, 'errors/500.html')


def not_found(request, exception):
    return render(request, 'errors/404.html', status=404)


def permission_denied(request, exception):
    return render(request, 'errors/403.html', status=403)


def bad_request(request, exception):
    return render(request, 'errors/400.html', status=400)
