from django.db.models import Count
from django.views.generic import ListView
from taggit.models import Tag

from catalog.models import Category, Manufacturer


class CategoryView(ListView):
    model = Category
    context_object_name = 'categories'
    template_name = 'catalog/category_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Category.objects.all().order_by('-name')


class ManufacturerView(ListView):
    model = Manufacturer
    context_object_name = 'manufacturers'
    template_name = 'catalog/manufacturer_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Manufacturer.objects.all().order_by('-name')


class TagView(ListView):
    model = Tag
    context_object_name = 'tags'
    template_name = 'catalog/tag_list.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = Tag.objects.all().order_by('-name')
        queryset2 = queryset.annotate(num_times=Count('taggit_taggeditem_items'))
        return queryset2

