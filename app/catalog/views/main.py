from itertools import chain
from operator import attrgetter

from django.shortcuts import render
from django.views.generic import ListView


from catalog.forms import SearchForm
from catalog.models import Product, Ingredient, get_ingredients


# buttons class for deciding which buttons to show on product page
from catalog.utils import inc_search


class Buttons:
    show_edit = False
    show_publish = False
    show_submit = False
    show_state = False
    show_moderate = False


def privacy(request):
    return render(request, 'catalog/privacy.html', {})


class ProductSearchView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'catalog/search_products.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        kwargs['form'] = SearchForm(initial={'q': self.q})
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        query = self.request.GET.get('q')
        self.q = query
        if query:
            inc_search(query)
            product_list = Product.objects.filter(name__icontains=query)\
                .filter(state='published')\
                .order_by('-name')
            ingredients = get_ingredients(query)
            result_list = sorted(
                chain(product_list, ingredients),
                key=attrgetter('name'))
            return result_list
        else:
            return Product.objects.all().filter(state='published').order_by('-name')

    def form_valid(self, form):
        # find a search and increment it for today's count
        return Product.objects.all().order_by('-name')

