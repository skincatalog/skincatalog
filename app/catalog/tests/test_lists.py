from django.test import TestCase
from django.urls import reverse

from catalog.tests.test_product import ProductTest


class TestManufacturerView(ProductTest):
    def setUp(self):
        super().setUp()
        url = reverse('catalog_product_by_manufacturer', kwargs={'pk': self.manufacturer.pk})
        self.response = self.client.get(url)

    def test_man_view_status_code(self):
        self.assertEqual(self.response.status_code, 200)


class TestManufacturerListView(ProductTest):
    def setUp(self):
        super().setUp()
        url = reverse('catalog_manufacturer_list')
        self.response = self.client.get(url)

    def test_man_list_status_code(self):
        self.assertEqual(self.response.status_code, 200)


class TestTagListView(ProductTest):
    def setUp(self):
        super().setUp()
        self.product.tags.add("red", "green", "fruit")
        url = reverse('catalog_tag_list')
        self.response = self.client.get(url)

    def test_tag_list_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_tag_list_count(self):
        self.assertEqual(self.response.context['object_list'].count(), 3)

    def test_tag_list_contains_tag(self):
        self.assertContains(self.response, 'green')
