from django.test import TestCase
from django.urls import reverse, resolve

from .test_product import ProductTest
from ..views import ProductHome


class HomeTests(ProductTest):
    def setUp(self):
        super().setUp()
        url = reverse('catalog_home')
        self.response = self.client.get(url)

    def test_home_view_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        view = resolve('/catalog/')
        self.assertEquals(view.func.view_class, ProductHome)

    def test_home_view_contains_link_to_product_page(self):
        product_detail_url = reverse('catalog_product', kwargs={'pk': self.product.pk})
        self.assertContains(self.response, 'href="{0}"'.format(product_detail_url))

    def test_home_view_no_link_to_moderated_product(self):
        product_detail_url = reverse('catalog_product', kwargs={'pk': self.product_mod.pk})
        self.assertNotContains(self.response, 'href="{0}"'.format(product_detail_url))


class ModTest(ProductTest):
    def setUp(self):
        super().setUp()
        url = reverse('catalog_product_moderated')
        self.response = self.client.get(url)

    def test_moderated_list_redirect_if_not_login(self):
        mod_page = reverse('catalog_product_moderated')
        self.assertEqual(self.response.status_code, 302)
