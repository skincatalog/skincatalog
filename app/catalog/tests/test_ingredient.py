from itertools import count

from django.test import TestCase
from django.urls import reverse, resolve

from catalog.models import Ingredient, Altname, get_ingredients, get_ingredient_search_results
from catalog.views import ProductByIngredientView


class IngredientTest(TestCase):
    def setUp(self):
        self.ingredient = Ingredient.objects.create(
            name='Aqua',
            description='<a href="https://www.google.com">Google</a>'
        )
        self.entry = [{'pk': 1, 'name': 'Aqua'}]
        self.alt_name = Altname.objects.create(name='Water', ingredient=self.ingredient)
        self.entry_two = [{'pk': 1, 'name': 'Aqua'}, {'pk': 1, 'name': 'Water'}]


class IngredientViewTests(IngredientTest):
    def setUp(self):
        super().setUp()
        url = reverse('catalog_ingredient', kwargs={'pk': self.ingredient.pk})
        self.response = self.client.get(url)

    def test_ingredient_view_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_ingredient_url_resolves_ingredient_view(self):
        view = resolve('/catalog/ingredient/1/')
        self.assertEquals(view.func.view_class, ProductByIngredientView)

    def test_ingredient_view_escaped(self):
        self.assertNotContains(self.response, '<a href="https://www.google.com">Google</a>')


class IngredientMethodTest(IngredientTest):
    def test_get_ingredients_none(self):
        self.assertEqual(0, get_ingredients('Tiger').count())

    def test_get_ingredients_one(self):
        self.assertEqual(1, get_ingredients('Aq').count())

    def test_get_ingredients_name(self):
        self.assertEqual(self.ingredient, get_ingredients('Aq').first())

    def test_get_ingredients_alt(self):
        self.assertEqual(self.ingredient, get_ingredients('Wat').first())

    def test_get_ingredients_search_one(self):
        self.assertEqual(1, len(get_ingredient_search_results('Aq')))

    def test_get_ingredients_search_two(self):
        self.assertEqual(2, len(get_ingredient_search_results('a')))

    def test_get_ingredients_search_name(self):
        self.assertEqual(self.entry, get_ingredient_search_results('Aq'))

    def test_get_ingredients_search_name_two(self):
        self.assertEqual(self.entry_two, get_ingredient_search_results('a'))
