from django.test import TestCase
from django.urls import reverse

from ..views import privacy


class ProductTests(TestCase):
    def setUp(self):
        super().setUp()
        url = reverse('privacy')
        self.response = self.client.get(url)

    def test_privacy_status_code(self):
        self.assertEqual(self.response.status_code, 200)
