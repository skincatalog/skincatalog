from django.test import TestCase
from django.urls import reverse, resolve

from ..models import Product, Category, Manufacturer


class ProductTest(TestCase):
    def setUp(self):
        self.category = Category.objects.create(name='Moisturiser')
        self.manufacturer = Manufacturer.objects.create(name='Grease')
        self.product = Product.objects.create(
            name='Balm',
            description='Skin Balm <a href="https://www.google.com">Google</a>',
            category=self.category,
            manufacturer=self.manufacturer,
            state='published'
        )
        self.product_mod = Product.objects.create(
            name='Balm mod',
            description='Skin Balm mod',
            category=self.category,
            manufacturer=self.manufacturer,
            state='moderated'
        )
