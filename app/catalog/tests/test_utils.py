from django.test import TestCase

from catalog.models import Search
from catalog.utils import inc_search


class UtilsTest(TestCase):
    def setUp(self):
        self.q = 'test'
        inc_search(self.q)

    def test_query_inc(self):
        s = Search.objects.filter(q=self.q).first()
        self.assertEqual(1, s.count)
