from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse, resolve

from membership.models import Member, Membership
from .test_product import ProductTest
from ..views import ProductView, ProductOwnerListView


class ProductTests(ProductTest):
    def setUp(self):
        super().setUp()
        self.client.logout()
        url = reverse('catalog_product', kwargs={'pk': self.product.pk})
        self.response = self.client.get(url)

    def test_product_view_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_product_url_resolves_product_view(self):
        view = resolve('/catalog/product/1/')
        self.assertEquals(view.func.view_class, ProductView)

    def test_product_view_escaped(self):
        self.assertNotContains(self.response, '<a href="https://www.google.com">Google</a>')

    def test_product_mod_view_status_code(self):
        self.client.logout()
        url_mod = reverse('catalog_product', kwargs={'pk': self.product_mod.pk})
        self.assertEqual(self.client.get(url_mod).status_code, 404)


class ProductModTests(ProductTest):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_superuser(username='john', email='john@doe.com', password='old_password', )
        # dummy request object as it is required with django-axes
        self.request = HttpRequest()
        self.client.login(request=self.request, username='john', password='old_password')
        url = reverse('catalog_product', kwargs={'pk': self.product_mod.pk})
        self.response = self.client.get(url)

    def test_product_view_status_code(self):
        self.assertEqual(self.response.status_code, 200)


class ProductOwnerViewTests(ProductTests):
    def setUp(self):
        super(ProductOwnerViewTests, self).setUp()
        self.password = 'horsestaple'
        self.user = User.objects.create_user(username='bob', email='bob@doe.com', password=self.password, )
        self.product_mod.owner = self.user.profile
        self.product_mod.save()
        # make user have publish rights
        membership = Membership.objects.get(name='Bronze')
        member = Member(user=self.user)
        member.membership = membership
        member.save()
        # dummy request object as it is required with django-axes
        self.request = HttpRequest()
        self.client.login(request=self.request, username='bob', password=self.password)
        url = reverse('catalog_product_by_owner', kwargs={'pk': self.user.pk})
        self.response = self.client.get(url)

    def test_product_url_resolves_my_view(self):
        view = resolve('/catalog/owner/1/')
        self.assertEquals(view.func.view_class, ProductOwnerListView)

    def test_product_list_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_product_list_contains_mod(self):
        self.assertIn(self.product_mod, self.response.context['object_list'])

    def test_product_list_not_contain_product(self):
        self.assertNotIn(self.product, self.response.context['object_list'])

    def test_product_view_product(self):
        url = reverse('catalog_product', kwargs={'pk': self.product_mod.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_product_view_product_published(self):
        url = reverse('catalog_product', kwargs={'pk': self.product.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

