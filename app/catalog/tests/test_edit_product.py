from django.contrib.auth.models import Group, User
from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse, resolve

from membership.models import Membership, Member
from ..forms import ProductForm
from ..models import Product, Category, Manufacturer


class ProductEditTest(TestCase):
    def setUp(self):
        self.category = Category.objects.create(name='Moisturiser')
        self.manufacturer = Manufacturer.objects.create(name='Grease')
        group, created = Group.objects.get_or_create(name='ProductCreators')

        self.user = User.objects.create_superuser(username='john', email='john@doe.com', password='old_password', )
        membership = Membership.objects.get(name='Bronze')
        member = Member(user=self.user)
        member.membership = membership
        member.save()
        # dummy request object as it is required with django-axes
        self.request = HttpRequest()
        self.client.login(request=self.request, username='john', password='old_password')

    def test_form(self):
        url = reverse('catalog_product_new')
        response = self.client.get(url)
        self.assertIsInstance(response.context['form'], ProductForm)
