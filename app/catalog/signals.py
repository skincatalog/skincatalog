import logging
from django.dispatch import receiver

from catalog.models import BuyLink
from external_urls.signals import external_click


@receiver(external_click)
def count_url_clicked_callback(sender, **kwargs):
    url = kwargs.get('url')
    # ip = kwargs.get('ip')
    # Get an instance of a logger
    # logger = logging.getLogger(__name__)
    # logger.info("tracked click to {} from {}".format(url, ip))
    try:
        buy_link = BuyLink.objects.get(url=url)
    except BuyLink.DoesNotExist:
        return
    buy_link.clicks += 1
    buy_link.save()
