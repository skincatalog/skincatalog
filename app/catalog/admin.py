from django import forms
from django.contrib import admin
from fsm_admin.mixins import FSMTransitionMixin

from .models import Product, Manufacturer, Category, Ingredient, Altname, Search, BuyLink
from .widgets import SearchableSelect


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ()
        widgets = {
            'ingredients': SearchableSelect(
                model='catalog.Ingredient',
                template_name='catalog/widget/ingredient_select.html',
                many=True,
                limit=10)
        }


class ProductAdmin(FSMTransitionMixin, admin.ModelAdmin):
    form = ProductForm
    # The name of one or more FSMFields on the model to transition
    fsm_field = ['state', ]
    list_display = (
        'name',
        'owner',
        'state',
    )
    list_filter = (
        'state',
    )
    readonly_fields = (
        'state',
    )


class IngredientAdmin(FSMTransitionMixin, admin.ModelAdmin):
    # The name of one or more FSMFields on the model to transition
    fsm_field = ['state', ]
    list_display = (
        'name',
        'state',
    )
    list_filter = (
        'state',
    )
    readonly_fields = (
        'state',
    )


class SearchAdmin(admin.ModelAdmin):
    list_display = (
        'q',
        'date',
        'count'
    )
    list_filter = (
        'date',
    )


class BuyLinkAdmin(admin.ModelAdmin):
    list_display = (
        'product',
        'name',
        'clicks'
    )


admin.site.register(Product, ProductAdmin)
admin.site.register(Manufacturer)
admin.site.register(Category)
admin.site.register(Ingredient, IngredientAdmin)
admin.site.register(Altname)
admin.site.register(Search, SearchAdmin)
admin.site.register(BuyLink, BuyLinkAdmin)
