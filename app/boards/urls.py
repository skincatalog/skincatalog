from django.urls import path
from . import views

urlpatterns = [
    path("",
         views.home, name="board_home"),
    path("<int:pk>/",
         views.TopicListView.as_view(), name="board_topics"),
    path("<int:pk>/new/",
         views.new_topic, name="board_new_topic"),
    path("<int:pk>/topics/<int:topic_pk>/",
         views.PostListView.as_view(), name="board_topic_posts"),
    path("<int:pk>/topics/<int:topic_pk>/reply/",
         views.reply_topic, name="board_reply_topic"),
    path("<int:pk>/topics/<int:topic_pk>/posts/<int:post_pk>/edit/",
         views.PostUpdateView.as_view(), name="board_edit_post"),
]
