from django.contrib import admin
from django.contrib.auth.models import Group


class ProductAdmin(admin.ModelAdmin):
    exclude = ('views', 'owner', 'group', )

    def save_model(self, request, obj, form, change):
        # TODO: make this a bit more flexible ref: #5
        obj.owner = request.user.profile
        obj.group = Group.objects.get('ProductCreators')
        obj.save()
