from django.contrib.auth.tokens import PasswordResetTokenGenerator

# extend PasswordResetTokenGenerator and override the hash value


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            str(user.pk) + str(timestamp) +
            str(user.profile.email_confirmed)
        )


account_activation_token = AccountActivationTokenGenerator()
