import requests
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import UpdateView
from django.conf import settings

from .forms import RegisterForm
from .tokens import account_activation_token


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            # begin reCAPTCHA
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()

            if result['success']:
                user = form.save(commit=False)
                user.is_active = False
                user.save()
                current_site = get_current_site(request)
                subject = 'Activate your Skin Catalog Account'
                message = render_to_string('account_activation_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                user.email_user(subject, message)
                return redirect('account_activation_sent')
            else:
                messages.error(request, 'Invalid reCAPTCHA. Please try again.')
    else:
        form = RegisterForm()
    recaptcha_site = settings.GOOGLE_CAPTCHA_SITE_KEY
    return render(request, 'accounts/register.html', {'form': form, 'recaptcha': recaptcha_site})


def account_activation_sent(request):
    return render(request, 'accounts/activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        # set membership
        if "membership" in settings.INSTALLED_APPS:
            from membership.models import Member, Membership
            try:
                membership = Membership.objects.get(pk=1)
                member = Member(user=user)
                member.membership = membership
                member.save()
            except Membership.DoesNotExist:
                pass
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        messages.info(request, "Activation Successful")
        return redirect('home')
    else:
        return render(request, 'accounts/account_activation_invalid.html')


@method_decorator(login_required, name='dispatch')
class UserUpdateView(UpdateView):
    model = User
    fields = ('first_name', 'last_name', 'email', )
    template_name = 'my_account.html'
    success_url = reverse_lazy('my_account')

    def get_context_data(self, **kwargs):
        kwargs['request'] = self.request
        return super().get_context_data(**kwargs)

    def get_object(self):
        return self.request.user
