from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from ..models import Profile
from ..tokens import account_activation_token
from ..views import register
from ..forms import RegisterForm


class RegisterTests(TestCase):
    def setUp(self):
        url = reverse('register')
        self.response = self.client.get(url)

    def test_register_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_register_url_resolves_register_view(self):
        view = resolve('/accounts/register/')
        self.assertEquals(view.func, register)

    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form, RegisterForm)

    def test_form_inputs(self):
        # only 5 input fields csrf, username, email and password *2
        self.assertContains(self.response, 'name="csrfmiddlewaretoken"', 1)
        self.assertContains(self.response, 'type="text"', 1)
        self.assertContains(self.response, 'type="email"', 1)
        self.assertContains(self.response, 'type="password"', 2)


class SuccessfulRegisterCase(TestCase):
    def setUp(self):
        url = reverse('register')
        settings.GOOGLE_CAPTCHA_SITE_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
        settings.GOOGLE_RECAPTCHA_SECRET_KEY = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
        data = {
            'username': 'john',
            'email': 'john@doe.com',
            'password1': 'abcdef123456',
            'password2': 'abcdef123456'
        }
        self.response = self.client.post(url, data)
        self.home_url = reverse('home')
        self.activate_email_url = reverse('account_activation_sent')


class SuccessfulRegisterTests(SuccessfulRegisterCase):
    def setUp(self):
        super().setUp()

    def test_redirection(self):
        """
        A valid form submission should redirect the user to the activate email sent page
        """
        self.assertRedirects(self.response, self.activate_email_url)

    def test_user_creation(self):
        self.assertTrue(User.objects.exists())

    def test_user_profile_creation(self):
        self.assertTrue(Profile.objects.exists())

    def test_user_authentication(self):
        """
        Create a new request to an arbitrary page.
        The resulting page should not have a user in its context,
        registration creates the user but does not log them in
        """
        response = self.client.get(self.home_url)
        user = response.context.get('user')
        self.assertFalse(user.is_authenticated)


class SuccessfulActivationTest(SuccessfulRegisterCase):
    def setUp(self):
        super().setUp()
        my_user = User.objects.get(email='john@doe.com')
        uid = urlsafe_base64_encode(force_bytes(my_user.pk))
        token = account_activation_token.make_token(my_user)
        activate_url = reverse('account_activate', kwargs={'uidb64': uid, 'token': token})
        self.response = self.client.get(activate_url)

    def test_user_activation(self):
        self.assertRedirects(self.response, self.home_url)

    def test_user_activate_status_code(self):
        self.assertEquals(self.response.status_code, 302)

    def test_user_activate_is_active(self):
        user = User.objects.get(email='john@doe.com')
        self.assertTrue(user.is_active)

    def test_user_activate_email_confirmed(self):
        my_user = User.objects.get(email='john@doe.com')
        self.assertTrue(my_user.profile.email_confirmed)

    def test_user_authentication_after_activate(self):
        response = self.client.get(self.home_url)
        user = response.context.get('user')
        self.assertTrue(user.is_authenticated)


class InvalidRegisterTests(TestCase):
    def setUp(self):
        url = reverse('register')
        self.response = self.client.post(url, {})  # submit empty fields

    def test_register_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_form_errors(self):
        form = self.response.context.get('form')
        self.assertTrue(form.errors)

    def test_dont_create_user(self):
        self.assertFalse(User.objects.exists())
