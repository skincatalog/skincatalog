FROM registry.gitlab.com/skincatalog/nginxpython:0-1-3

WORKDIR /var/www/webroot/ROOT
COPY app ./
COPY website_config/deploy-post.sh /var/www/webroot/deploy-post.sh

RUN . /var/www/webroot/virtenv/bin/activate && \
    python ../get-poetry.py && \
    . /var/www/webroot/.poetry/env && \
    poetry config virtualenvs.create false && \
    poetry env info && \
    poetry install
