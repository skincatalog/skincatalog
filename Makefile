init:
	pip install poetry
	cd app && \
		poetry install

dev: init
	cd app && \
		export SECRET_KEY="some-random-value-420" && \
		export RECAPTCHA_SECRET_KEY="some-random-value-420" && \
		export GOOGLE_ANALYTICS="" && \
		export DATABASE_URL="sqlite:///db.sqlite3" && \
		export ALLOWED_HOSTS="localhost" && \
		export STATIC_ROOT="" && \
		poetry run python manage.py migrate && \
		poetry run python manage.py runserver --settings=skincatalog.settings.development

coverage: init
	cd app && \
		export SECRET_KEY="some-random-value-420" && \
		export RECAPTCHA_SECRET_KEY="some-random-value-420" && \
		export GOOGLE_ANALYTICS="" && \
		export DATABASE_URL="sqlite:///db.sqlite3" && \
		export ALLOWED_HOSTS="localhost" && \
		export STATIC_ROOT="" && \
		export DJANGO_SETTINGS_MODULE=skincatalog.settings.development && \
		poetry run python manage.py migrate && \
        poetry run coverage run --source='.' manage.py test && \
        poetry run coverage report
